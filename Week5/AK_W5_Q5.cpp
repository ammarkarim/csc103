// Week 4 Q3


#include <iostream> 
#include <algorithm> 
#include <vector>

using namespace std;

void custom_merge(vector<int> &A, vector<int> &B, vector<int> &start_vector);
void c_mergesort(vector<int> & vec);

void c_mergesort(int arr[], int l, int r);

int main()
{	
	int input;
	vector<int> init_vector;

	while (input != 0){
        cout << "We are pushing integers to init_vector. Enter a number. enter 0 to exit" << endl;
        cin >> input;
        if (input == 0) 
            continue;

        init_vector.push_back(input);
    }

    input = -1;
    cout << endl;

    c_mergesort(init_vector);

    vector<int>::iterator k = init_vector.begin();
    //print out merged and sorted vector
	while(k != init_vector.end()) {
		cout << *k << " ";
		k++;
	}
	cout << endl;
	
    return 0;
}

void c_mergesort(vector<int> &vec) {
	if (vec.size() == 1)
		return;

	int middle = vec.size() / 2;
	vector<int> l;
	vector<int> r;

	for (int i=0; i < middle; i++)
		l.push_back(vec[i]);

	for (int j=0; j < vec.size()-middle; j++) 
		r.push_back(vec[j+middle]);

	c_mergesort(l);
	c_mergesort(r);

	custom_merge(l, r, vec);
	
}

void custom_merge(vector<int> &A, vector<int> &B, vector<int> &start_vector){
	vector<int>::iterator i = A.begin();
	vector<int>::iterator j = B.begin();
	vector<int>::iterator k = start_vector.begin();


	while (i != A.end() && j != B.end()) {
		if (*i < *j){
			*k = *i;
			i++;
		}
		else {
			*k = *j;
			j++;
		}
		k++;
	}

	while(i != A.end()) {
		*k = *i;
		i++;
		k++;
	}

	while(j != B.end()) {
		*k = *j;
		j++;
		k++;
	}
	return;
}

