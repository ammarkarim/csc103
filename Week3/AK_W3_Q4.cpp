// Week 3 Q4


#include<iostream> 
#include <algorithm>
using namespace std;

int main()
{	
    int count=0;
    string input;
    int rotations;
    bool right;
    string A[2][2];

    for (int i=0; i<2;i++){
        for (int j=0; j<2; j++){
            cout << "enter a letter" << endl;
            cin >> A[i][j];
        }
        cout << endl;
    }

    cout << "enter the number of rotations" << endl;
    cin >> rotations;

    cout << "before" << endl;

    for (int i=0; i<2;i++){
        for (int j=0; j<2; j++){
            cout << A[i][j] << " ";  
        }
        cout << endl;
    }


    if (rotations > 1)
        right = true;
    else
        right = false;

    rotations = abs(rotations);

    for (int l=0; l<rotations; l++){
        string first = A[0][0];
        string second = A[0][1];
        string third = A[1][0];
        string fourth = A[1][1];


        if (right) {
            A[0][0] = third;
            A[0][1] = first;
            A[1][0] = fourth;
            A[1][1] = second;
        }
        else {
            A[0][0] = second;
            A[0][1] = fourth;
            A[1][0] = first;
            A[1][1] = third;
        }
    }
    	
    cout << "after " << rotations << " rotations" << endl;

    for (int i=0; i<2;i++){
        for (int j=0; j<2; j++){
            cout << A[i][j] << " ";  
        }
        cout << endl;
    }
    return 0;
}