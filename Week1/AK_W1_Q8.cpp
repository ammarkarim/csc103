// Week 1 Q8

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	int a, b, c;
	int greatest;

	a = 4, b = 12, c = 3;

	if (a >= b) {
		if (a >= c) 
			greatest = a;
		else 
			greatest = c;
	}
	else if (b >= c)
		greatest = b;
	else 
		greatest = c;

	cout << "the greatest is " << greatest << endl;
	
    return 0;
}