// Week 3 Q2


#include<iostream> 
#include <algorithm>
using namespace std;

int main()
{	
	int maxSize;
    int count=0;
    int runStart=0, runEnd=0, runMax=0;
    int tempStart=0, tempEnd=0, tempMax=0;
    string input;
    
    cout << "enter max size of array " << endl;
    cin >> maxSize;

    string A [maxSize];

    while (count < maxSize && input !="#"){
        cout << "Enter a letter" << endl;
        cin >> input;

        if (input == "#") 
            continue;

        A[count] = input;
        count++;
    }

    for (int i=0,j=0; i<count; i++){
        while (A[i] == A[j]){
            if (A[i] == A[j]){
                tempMax++;
                if (tempMax > runMax){
                    runMax = tempMax;
                    runStart = i;
                    runEnd = j;
                }
            }

            j++;
        }

        tempMax=0;
    }

    cout << "For the longest run, the start index is " << runStart << " and the end index is " << runEnd << " and the length is " << runMax << endl;
	
    return 0;
}