// Week 5 Q1
#include<iostream> 
#include "AK_W5_Q1.h"

using namespace std;


Point::Point () {
    set_X(0);
    set_Y(0);
}

Point::Point (double x_val, double y_val) {
    set_X(x_val);
    set_Y(y_val);
}

Point::~Point () {
}

void Point::set_X(double x_val){
    x = x_val;
}

void Point::set_Y(double y_val){
    y = y_val;
}

double Point::get_X(){
    return x;
}

double Point::get_Y() {
    return y;
}

void Point::print() {
    cout << "(" << get_X() << "," << get_Y() << ")" << endl;
}

string Point::quadrant () {
    string result;
    if (x == 0 && y ==0)
        result = "origin";
    else if (x == 0)
        result = "y-axis";
    else if (y == 0)
        result = "x-axis";

    if (x > 0 && y > 0)
        result = "quadrant I";
    if (x < 0 && y > 0)
        result = "quadrant II";
    if (x < 0 && y < 0)
        result = "quadrant III";
    if (x > 0 && y < 0)
        result = "quadrant IV";

    return result;
}

Point Point::operator+ (Point& addend) {
    Point temp;
    temp.x = x + addend.x;
    temp.y = y + addend.y;
    return temp;
}

Point Point::operator- (Point& addend) {
    Point temp;
    temp.x = x - addend.x;
    temp.y = y - addend.y;
    return temp;
}



