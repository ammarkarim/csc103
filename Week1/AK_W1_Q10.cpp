// Week 1 Q10

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	int choice;
	double a, b, area;

	cout << "What area would would you like to calculate?" << endl;
	cout << "Enter 1 for circle" << endl;
	cout << "Enter 2 for triangle" << endl;
	cout << "Enter 3 for square" << endl;
	cout << "Enter 4 for rectangle" << endl;

	cin >> choice;

	switch(choice) {
		case 1:
			cout << "enter radius" << endl;
			cin >> a;

			area = 3.14 * a * a;
			cout << "the area of the cirle is " << area << endl;
			break;
		case 2:
			cout << "enter base" << endl;
			cin >> a;
			cout << "enter height" << endl;
			cin >> b;

			area = .5 * a * b;

			cout << "the area of the triangle is " << area << endl;
			break;
		case 3:
			cout << "enter side length" << endl;
			cin >> a;

			area = a * a;
			cout << "the area of the sqaure is " << area << endl;
			break;
		case 4:
			cout << "enter height " << endl;
			cin >> a;
			cout << "enter width " << endl;
			cin >> b;

			area = a * b;
			cout << "the area of the rectangle is " << area << endl;
			break;
		default:
			cout << "invalid choice" << endl;
	}
    return 0;
}