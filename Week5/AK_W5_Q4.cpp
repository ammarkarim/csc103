// Week 5 Q4


#include<iostream> 
using namespace std;


bool recursive_palindrome(int start, int end, string s);


int main()
{   
   string test = "racecar";

   cout << recursive_palindrome(0, test.length()-1, test) << endl;

    return 0;
}


bool recursive_palindrome(int start, int end, string s){
  if (start >= end)
    return true;
  if (s[start] != s[end])
    return false;
  return recursive_palindrome(++start, --end, s);
}