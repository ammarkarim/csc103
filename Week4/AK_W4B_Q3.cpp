// Week 4 Q3


#include <iostream> 
#include <algorithm> 
#include <vector>

using namespace std;

vector<int> custom_merge(vector<int> A, vector<int> B, vector<int> MSvector);

int main()
{	
	int input;
	vector<int> init_vector;
	vector<int> MSvector;
	vector<int> A;
	vector<int> B;

	while (input != 0){
        cout << "We are pushing integers to A. Enter a number. enter 0 to exit" << endl;
        cin >> input;
        if (input == 0) 
            continue;

        A.push_back(input);
    }

    input = -1;
    cout << endl;

    while (input != 0){
        cout << "We are pushing integers to B. Enter a number. enter 0 to exit" << endl;
        cin >> input;
        if (input == 0) 
            continue;

        B.push_back(input);
    }

    input = -1;

    MSvector = custom_merge(A, B, init_vector);

    vector<int>::iterator k = MSvector.begin();
    //print out merged and sorted vector
	while(k != MSvector.end()) {
		cout << *k << " ";
		k++;
	}
	cout << endl;
	
    return 0;
}

vector<int> custom_merge(vector<int> A, vector<int> B, vector<int> start_vector){
	sort(A.begin(), A.end());
	sort(B.begin(), B.end());

	vector<int>::iterator i = A.begin();
	vector<int>::iterator j = B.begin();


	while (i != A.end() && j != B.end()) {
		if (*i < *j){
			start_vector.push_back(*i);
			i++;
		}
		else {
			start_vector.push_back(*j);
			j++;
		}
	}

	while(i != A.end()) {
		start_vector.push_back(*i);
		i++;
	}

	while(j != B.end()) {
		start_vector.push_back(*j);
		j++;
	}

	return start_vector;
}

