// Week 2 Q7


#include<iostream> 
using namespace std;


void change(int dollars, int &hundreds, int &fifties, int &twenties, int &tens, int &fives, int &ones);

int main()
{   
    int dollars;
    int  hundreds=0, fifties=0, twenties=0, tens=0, fives=0, ones=0;


    cout << "enter dollars: ";
    cin >> dollars;
    
    change(dollars, hundreds, fifties, twenties, tens, fives, ones);

    cout << hundreds << " hundreds" << endl;
    cout << fifties << " fifties" << endl;
    cout << twenties << " twenties" << endl;
    cout << tens << " tens" << endl;
    cout << fives << " fives" << endl;
    cout << ones << " ones" << endl;


    return 0;
}


void change(int dollars, int &hundreds, int &fifties, int &twenties, int &tens, int &fives, int &ones) {
    hundreds = dollars / 100;
    dollars = dollars - (100 * hundreds);

    if (dollars >= 50){
        fifties = 1;
        dollars = dollars - 50;
    }

    tens = dollars / 10;
    dollars = dollars - (10 * tens);

    if (dollars >= 5){
        fives = 1;
        dollars = dollars - 5;
    }

    ones = dollars;
}