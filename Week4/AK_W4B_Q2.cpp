// Week 4 Q2


#include <iostream> 
#include <algorithm> 
#include <vector>

using namespace std;

vector<int> custom_insertion_sort(vector<int> A);

int main()
{	
	int input;
	vector<int> A;
	vector<int> MSvector;


	while (input != 0){
        cout << "We are pushing integers to A. Enter a number. enter 0 to exit" << endl;
        cin >> input;
        if (input == 0) 
            continue;

        A.push_back(input);
    }

    input = -1;
    cout << endl;


    MSvector = custom_insertion_sort(A);

    vector<int>::iterator k = MSvector.begin();
    //print out merged and sorted vector
	while(k != MSvector.end()) {
		cout << *k << " ";
		k++;
	}
	cout << endl;
	
    return 0;
}

vector<int> custom_insertion_sort(vector<int> A){
	vector<int>::iterator i = A.begin();
	vector<int>::iterator j;

	while(i != A.end()) {
		j = i;
		while(j != A.begin() && *(j-1) > *j){
			std::iter_swap(j, j-1);
			j--;
		}
		i++;
	}

	return A;
}

