// Week 2 Q1


#include<iostream> 
using namespace std;

int main()
{	
	for (int number=0; number <= 100; number++) {
        if (number % 3 == 0  && number % 5 == 0) 
            cout << number << " is " << "FizzBuzz" << endl;
        else if (number % 3 == 0)
            cout << number << " is " << "Fizz" << endl;
        else if (number % 5 == 0)
            cout << number << " is " << "Buzz" << endl;
    }
	
    return 0;
}