// Week 1 Q5

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	double f, C;

    f = 100;
    C = (f - 32)/1.8;

    cout << f << " degrees Fahrenheit is equivalent to " << C << " degrees Celcius." << endl;
	
    return 0;
}