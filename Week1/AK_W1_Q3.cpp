// Week 1 Q3

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	double f, C;

    // C = 100;
    f = (double(9.0/5.0) * 100) + 32;

    cout << "100 degrees Celsius is equivalent to " << f << " degrees Fahrenheit." << endl;
	
    return 0;
}