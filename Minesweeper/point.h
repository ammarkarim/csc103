#ifndef POINT_H
#define POINT_H

#include <string>
 

class Point {
    char x, y;
public:
    Point();
    Point(char,char);
    ~Point();
    void set_X(char);
    void set_Y(char);
    char get_X();
    char get_Y();
    void print();
    friend std::istream& operator >> (std::istream& in, Point&);
    friend std::ostream& operator << (std::ostream& out, Point&);
    bool operator == (Point&);
};
 

#endif