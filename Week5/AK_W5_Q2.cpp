// Week 5 Q1
#include<iostream>
#include <cmath>
#include "AK_W5_Q1.h"
#include "AK_W5_Q2.h"

using namespace std;


Line::Line () {
    p1.set_X(0);
    p1.set_Y(0);

    p2.set_X(0);
    p2.set_Y(0);
}

Line::Line (Point p1_val, Point p2_val) {
    p1 = p1_val;
    p2 = p2_val;
}

Line::~Line () {
}

void Line::set_P1(double x_val, double y_val){
    p1.set_X(x_val);
    p1.set_Y(y_val);
}

void Line::set_P2(double x_val, double y_val){
    p2.set_X(x_val);
    p2.set_Y(y_val);
}

double Line::get_P1_X(){
    return p1.get_X();
}

double Line::get_P1_Y() {
    return p1.get_Y();
}

double Line::get_P2_X(){
    return p2.get_X();
}

double Line::get_P2_Y() {
    return p2.get_Y();
}

void Line::print() {
    cout << "(" << p1.get_X() << "," << p1.get_Y() << ") to ";
    cout << "(" << p2.get_X() << "," << p2.get_Y() << ")" << endl;
}

bool Line::isParallel(Line l2) {
    double slope1 = (get_P2_Y() - get_P1_Y()) / (get_P2_X() - get_P1_X());
    double slope2 = (l2.get_P2_Y() - l2.get_P1_Y()) / (l2.get_P2_X() - l2.get_P1_X());

    if (slope1 == slope2)
        return true;
    else 
        return false;

}

bool Line::operator> (Line& l) {
    double sq = 2.0;
    double length1, length2;

    length1 = sqrt(pow(get_P1_X() - get_P2_X(), 2) + pow(get_P1_Y() - get_P2_Y(), 2));

    length2 = sqrt((pow((l.get_P1_X() - l.get_P2_X()), sq)) + ((pow((l.get_P1_Y() - l.get_P2_Y()), sq))));

    if (length1 > length2)
        return true;
    else
        return false;

}

bool Line::operator< (Line& l) {
    double sq = 2.0;
    double length1, length2;
    bool greater;

    length1 = sqrt((pow((get_P1_X() - get_P2_X()), sq)) - ((pow((get_P1_Y() - get_P2_Y()), sq))));

    length2 = sqrt((pow((l.get_P1_X() - l.get_P2_X()), sq)) - ((pow((l.get_P1_Y() - l.get_P2_Y()), sq))));

    if (length1 < length2)
        return true;
    else
        return false;

}




int main()
{	
    Line l1;
    l1.print();

    Point p1(3, 0);
    Point p2(3, 11);

    Line l2(p1, p2);
    l2.print();

    Point p3(0, 0);
    Point p4(0, 9);

    Line l3(p3, p4);
    l3.print();

    
    if (l2.isParallel(l3))
        cout << "l2 and l3 are parallel" << endl;
    else
        cout << "l2 and l3 are not parallel" << endl;

    if (l2 > l3)
        cout << "l2 is longer than l3" << endl;
    else 
        cout << "l2 is smaller than l3" << endl;
  
    return 0;
}

