// Week 1 Q6

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	int number;

	cout << "enter number" << endl;
	cin >> number;

	if (number % 2 == 1) 
		cout << number << " is odd" << endl;
	else
		cout << number << " is even" << endl;
	
    return 0;
}