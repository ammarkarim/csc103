#include<iostream> 
#include "point.h"

using namespace std;


Point::Point () {
    set_X('A');
    set_Y('A');
}

Point::Point (char x_val, char y_val) {
    set_X(x_val);
    set_Y(y_val);
}

Point::~Point () {
}

void Point::set_X(char x_val){
    x = x_val;
}

void Point::set_Y(char y_val){
    y = y_val;
}

char Point::get_X(){
    return x;
}

char Point::get_Y() {
    return y;
}


std::istream& operator>> (std::istream &in, Point& p) {
    cout << "Enter X coordinate ";
    in >> p.x;
    cout << "Enter Y coordinate ";
    in >> p.y;
    return in;
}

std::ostream& operator<< (std::ostream &out, Point& p) {
    out << "(" << p.x << "," << p.y << ")" << endl;
    return out;
}

bool Point::operator== (Point& p) {
    return (x == p.x && y == p.y);
}



