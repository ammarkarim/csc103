#ifndef MAP_H
#define MAP_H

#include <string>
#include "point.h"
#include "mines.h"
 

class Map {
    int length, width;
    char** solution;
    char** currentMap;
    char** entered;
 
    char lengthLabel[37] = {'A','B','C','D','E','F','G','H','I','J','K','L','M',
    'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
    char widthLabel[37] = {'A','B','C','D','E','F','G','H','I','J','K','L','M',
    'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};

public:
    Map();
    Map(int,int);
    ~Map();
    void printSolution();
    void printCurrentMap();
    void printEntered();
    void generateSolution(Mines*,int);
    void generateSolution();    
    void generateCurrent();
    void generateEntered();
    char** getSolutionMap();
    int getMapLength();
    int getMapWidth();
    bool checkIfEntered(char,char);
    void updateCurrentMap(char,char,char);
    void updateEntered(char,char);
    bool containsMine(char,char);
    bool isGameWon();
    int getXcoord(char);
    int getYcoord(char);
};
 

#endif