// Week 4 Q5


#include<iostream> 

using namespace std;

struct Node {
        int data;
        Node *next;
};


void start_list(Node* head, int value) {
    head->data = value;
    head->next = NULL;
}

void add_node(Node *head, int value) {
    Node *new_node = new Node;
    new_node->data = value;
    new_node->next = NULL;

    Node *cur_node = head;
    while(cur_node){
        if (cur_node->next == NULL) {
            cur_node->next = new_node;
            return;
        }
        cur_node = cur_node->next;
    }
    
    
}

void delete_last_node(Node *head) {
    Node *cur_node = head;
    while(cur_node->next && cur_node->next->next)
        cur_node = cur_node->next;
    cur_node->next = NULL;
}

void print_elements(Node *head) {
    Node *cur_node = head;
    while(cur_node){
        cout << cur_node->data << " ";
        cur_node = cur_node->next;
    }
    cout << endl;
}

void delete_all_elements(Node **head) {
    Node *temp;
    while (*head) {
        temp = *head;
        *head = temp->next;
        delete temp;
    }
}

int size_of_LL(Node *head) {
    Node *cur_node = head;
    int count = 0;
    while(cur_node){
        count++;
        cur_node = cur_node->next;
    }
    return count;
}


int main()
{	
    Node *new_list = new Node;

    start_list(new_list, 4);
    add_node(new_list, 5);
    add_node(new_list, 2);
    add_node(new_list, 7);

    cout << "original list:" << endl;
    print_elements(new_list);
    cout << "size is " << size_of_LL(new_list) << endl << endl;

    delete_last_node(new_list);

    cout << "after delete last element:" << endl;
    print_elements(new_list);
    cout << "size is " << size_of_LL(new_list) << endl << endl;

    delete_all_elements(&new_list);

    cout << "after delete all elements:" << endl;
    print_elements(new_list);
    cout << "size is " << size_of_LL(new_list) << endl << endl;

	
    return 0;
}

