// Week 2 Q7


#include<iostream> 
using namespace std;


int pow2(int b,int n);

int main()
{   
    int n;
    int b;
    double result;

    cout << "enter b: ";
    cin >> b;

    cout << "enter n: ";
    cin >> n;
    
    result = pow2(b, n);

    cout << "the result is " << result << endl;
    return 0;
}

int pow2(int b, int n){
    int result=1;

    for (int i=0; i < n; i++){
        result  = result * b;
    }
    return result;
}