// Week 2 Q8
#include<iostream> 
using namespace std;

string quadrant(int x,int y);

int main()
{   
    int x;
    int y;
    string result;

    cout << "enter x: ";
    cin >> x;

    cout << "enter y: ";
    cin >> y;
    
    result = quadrant(x, y);

    cout << "The coordinates are located " << result << endl;
    return 0;
}

string quadrant(int x, int y){
    string result;
    if (x == 0 && y ==0)
        result = "at the origin";
    else if (x == 0)
        result = "on the y-axis";
    else if (y == 0)
        result = "on the x-axis";

    if (x > 0 && y > 0)
        result = "in quadrant I";
    if (x < 0 && y > 0)
        result = "in quadrant II";
    if (x < 0 && y < 0)
        result = "in quadrant III";
    if (x > 0 && y < 0)
        result = "in quadrant IV";

    return result;
}