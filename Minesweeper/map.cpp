#include<iostream> 
#include "map.h"


using namespace std;


Map::Map () {
    length = 0;
    width = 0;

}

Map::Map (int l, int w) {
    length = l;
    width = w;

    generateSolution();
    generateCurrent();
    generateEntered();

    printCurrentMap();
}

Map::~Map () {
    // delete solution;
    // delete currentMap;
    // delete entered;
}

void Map::printSolution(){
    cout << "  ";
    for (int m=0; m<length; m++){
        cout << lengthLabel[m] << " ";
    }
    cout << endl;
    for (int i=0; i<length; i++) {
        cout << widthLabel[i] << " ";
        for (int j=0; j<width; j++) {
            cout << solution[i][j] << " ";
        }
        cout << endl;
    }
}

void Map::printCurrentMap(){
    cout << "  ";
    for (int m=0; m<length; m++){
        cout << lengthLabel[m] << " ";
    }
    cout << endl;
    for (int i=0; i<length; i++) {
        cout << widthLabel[i] << " ";
        for (int j=0; j<width; j++) {
            cout << currentMap[i][j] << " ";
        }
        cout << endl;
    }
}

void Map::printEntered(){
    cout << "  ";
    for (int m=0; m<length; m++){
        cout << lengthLabel[m] << " ";
    }
    cout << endl;
    for (int i=0; i<length; i++) {
        cout << widthLabel[i] << " ";
        for (int j=0; j<width; j++) {
            cout << entered[i][j] << " ";
        }
        cout << endl;
    }
}

void Map::generateSolution(){
    solution = new char*[length];
    for (int i=0; i<length;i++) {
        solution[i] = new char[width];
    }

    for(int m=0;m<length;m++){
        for (int n=0;n<width;n++){
            if (solution[m][n] != 'M')
                solution[m][n] = ' ';
        }
    }
}

void Map::generateSolution(Mines* mines, int numberOfMines){
    solution = new char*[length];

    for (int i=0; i<length;i++) {
        solution[i] = new char[width];
    }

    char x,y;
    int x_coord, y_coord;

    for (int j=0; j<numberOfMines;j++) {
        x = mines[j].get_X();
        y = mines[j].get_Y();
        x_coord = getXcoord(x);
        y_coord = getXcoord(y);

        solution[x_coord][y_coord] = 'M';
    }
    for(int m=0;m<length;m++){
        for (int n=0;n<width;n++){
            if (solution[m][n] != 'M')
                solution[m][n] = ' ';
        }
    }

  
}

void Map::generateCurrent() {
    currentMap = new char*[length];
    for (int i=0; i<length;i++) {
        currentMap[i] = new char[width];
    }

    for(int m=0;m<length;m++){
        for (int n=0;n<width;n++){
            if (currentMap[m][n] != 'M')
                currentMap[m][n] = ' ';
        }
    }
}

void Map::generateEntered() {
    entered = new char *[length];
    for (int i=0; i<length;i++) {
        entered[i] = new char[width];
    }

    for(int m=0;m<length;m++){
        for (int n=0;n<width;n++){
            if (entered[m][n] != 'i')
                entered[m][n] = ' ';
        }
    }
}



int Map::getMapLength() {
    return length;
}

int Map::getMapWidth() {
    return width;
}

bool Map::checkIfEntered(char x, char y) {
    int x_coord,y_coord;
    x_coord = getXcoord(x);
    y_coord = getXcoord(y);
    if (entered[x_coord][y_coord] == 'e')
        return true;
    else {
        return false;
    }
}

void Map::updateEntered(char x,char y){
    int x_coord,y_coord;
    x_coord = getXcoord(x);
    y_coord = getXcoord(y);

    entered[x_coord][y_coord] = 'i';
}

void Map::updateCurrentMap(char x,char y, char in){
    int x_coord,y_coord;
    x_coord = getXcoord(x);
    y_coord = getXcoord(y);

    if (in == 'r'){
        if(containsMine(x,y))
            currentMap[x_coord][y_coord] = 'm';
        else
            currentMap[x_coord][y_coord] = 'e';
    }
    else
        currentMap[x_coord][y_coord] = 'm';
}

bool Map::isGameWon(){
    for (int i=0;i<length;i++){
        for(int j=0;j<width;j++) {
            if (solution[i][j] == 'M' && entered[i][j] != 'e')
                return false;
        }
    }
    return true;
}

bool Map::containsMine(char x, char y) {
    int x_coord,y_coord;
    x_coord = getXcoord(x);
    y_coord = getXcoord(y);
    if (solution[x_coord][y_coord] == 'M')
        return true;
    else
        return false;
}



int Map::getXcoord(char x) {
    int x_index;

    for (int i=0;i<36;i++){
        if (lengthLabel[i] == x)
            x_index = i;
    }

    return x_index;
}

int Map::getYcoord(char y) {
    int y_index;

    for (int i=0;i<36;i++){
        if (lengthLabel[i] == y)
            y_index = i;
    }
    
    return y_index;
}


