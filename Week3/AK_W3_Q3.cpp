// Week 3 Q3


#include<iostream> 
#include <algorithm>
using namespace std;

int main()
{	
	int maxSize;
    int count=0;
    string  temp;
    int shift;
    string input;
    
    cout << "enter max size of array " << endl;
    cin >> maxSize;

    string A [maxSize];

    while (count < maxSize && input !="#"){
        cout << "Enter a letter" << endl;
        cin >> input;

        if (input == "#") 
            continue;

        A[count] = input;
        count++;
    }
    
    cout << "enter the number of shifts" << endl;
    cin >> shift;

    for (int i=0; i<shift; i++){ 
        temp = A[count-1];
        for (int j=count-1;j>=0; j--){
            A[j+1] = A[j];
        }
        A[0] = temp;
    }

    for (int k=0; k<count; k++){
        cout << A[k] << " ";
    }
    cout << endl;

    	
    return 0;
}