#ifndef LINE_H
#define LINE_H

#include "AK_W5_Q1.h"

class Line {
    Point p1, p2;
public:
    Line();
    Line(Point,Point);
    ~Line();
    void set_P1(double,double);
    void set_P2(double,double);
    double get_P1_X();
    double get_P1_Y();
    double get_P2_X();
    double get_P2_Y();
    bool isParallel(Line);
    void print();
    bool operator > (Line&);
    bool operator < (Line&);
};
 

#endif