// Week 2 Q7


#include<iostream> 
using namespace std;


void sort(int &a, int &b, int &c);

int main()
{   
    int a;
    int b;
    int c;


    cout << "enter a: ";
    cin >> a;

    cout << "enter b: ";
    cin >> b;

    cout << "enter c: ";
    cin >> c;
    
    sort(a, b, c);

    cout << a << " " << b << " " << c << endl;
    return 0;
}

void sort(int &a, int &b, int &c){
    int temp;

    if (a > c){
        temp = a;
        a = c;
        c = temp;
    }

    if (a > b) {
        temp = a;
        a = b;
        b = temp;
    }

    if (b > c) {
        temp = b;
        b = c;
        c = temp;
    }
}