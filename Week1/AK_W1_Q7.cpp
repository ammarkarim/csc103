// Week 1 Q7

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	int number;

	cout << "enter number" << endl;
	cin >> number;

	if (number % 3 == 0  && number % 5 == 0) 
		cout << "FizzBuzz" << endl;
	else if (number % 3 == 0)
		cout << "Fizz" << endl;
	else if (number % 5 == 0)
		cout << "Buzz" << endl;
	
    return 0;
}