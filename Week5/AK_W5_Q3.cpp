// Week 5 Q3


#include<iostream> 
using namespace std;


int iterative_triangle(int n);
int recursive_triangle(int n);

int main()
{   
   int tri, count;
   count = 3;

   tri = iterative_triangle(count);

   cout << tri << endl;

    return 0;
}


int iterative_triangle(int n){
    int triangle_number = 0;
    for (int i=0; i < n+1; i++) {
        triangle_number += i;
    }
    return triangle_number;
}

int recursive_triangle(int n) {
    if (n == 1)
        return n;
    return n + recursive_triangle(n-1);
}