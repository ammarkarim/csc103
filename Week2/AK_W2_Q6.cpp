// Week 2 Q6


#include<iostream> 
#include <stack>
using namespace std;

int main()
{   
    stack<int> digits;
    int n;
    int digit;

    cout << "enter n: ";
    cin >> n;

    while (n != 0) {
        digit = n % 10;
        digits.push(digit);

        n = n / 10;
    }

    while (!digits.empty()){
        cout << digits.top() << endl;
        digits.pop();
    } 
    return 0;
}