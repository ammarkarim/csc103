// Week 1 Q2

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	string firstName, lastNameInitial;
	int age, bodyTemp, zipcode;
	bool alive;


    cout << "First Name? " << endl;
    cin >> firstName;
    cout << "Last Name Initial?" << endl;
    cin >>  lastNameInitial;
    cout << "Age?" << endl;
    cin >> age;
    cout << "Body Temp?" << endl;
    cin >> bodyTemp;
    cout << "Alive? enter 0 or 1" << endl;
    cin >> alive;
    cout << "Zip Code?" << endl;
    cin >> zipcode;

    cout << "First Name: " << firstName << endl;
    cout << "Last Name Initial: " << lastNameInitial << endl;
    cout << "Age: " << age << endl;
    cout << "Body Temp: " << bodyTemp << endl;
    cout << "Alive: " << alive << endl;
    cout << "Zip Code: " << zipcode << endl;
	
    return 0;
}