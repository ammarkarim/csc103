#include <iostream> 
#include "point.h"
#include "mines.h"
#include "game.h"

using namespace std;

int main() 
{
	int input;
	cout << "1. Start a new game \n" 
		 << "2. Load an Existing Game \n"
		 << "3. Help \n" 
		 << "4. Quit" << endl;

	cin >> input;

	if(input == 1){
		int size;
		int length, width, mines;
		char x,y,mark;
		bool cont=true;
		Game g;

		cout << "Enter the size of the map" << endl;
		cout << "1. Small \n"
			 << "2. Medium \n"
			 << "3. Large \n"
			 << "4. Custom" << endl;

		cin >> size;
		if (size == 1){
			Game temp(8,8,10);
			g = temp;
		}
		else if(size == 2){
			Game temp(16,16,40);
			g = temp;
		}
		else if(size == 3){
			Game temp(30,16,99);
			g = temp;
		}
		else if(size == 4){
			cout << "Enter length" << endl;
			cin >> length;
			cout << "Enter width" << endl;
			cin >> width;
			cout << "Enter number of mines" << endl;
			cin >> mines;

			Game temp(length,width,mines);
			g = temp;
		}

		do {
			cout << "Enter x" << endl;
			cin >> x;
			x= std::toupper(x);

			cout << "Enter y" << endl;
			cin >> y;
			y= std::toupper(y);

			cout << "Enter r to reveal or m to mark as mine" << endl;
			cin >> mark;

			if(mark != 'r' && mark != 'm'){
				cout << "You must enter either r or m" << endl;
				continue;
			}

			if(g.checkEntered(x,y)){
				cout << "You already entered this" << endl;
				continue;
			}
			else 
				g.enterCoord(x,y,mark);

			if(g.isGameWon()){
				cout << "congrats you won the game!" << endl;
				cont = false;
			}

			if (g.healthZero()){
				cout << "You lost all your health. Goodbye" << endl;
				cont = false;
			}

			} while(cont);
		}
		else {
			cout << "the other options dont work yet :(...try 1" << endl;
		}

	

	// cout << "1. Play \n" 
	// 	 << "2. Print {map || score} \n"
	// 	 << "3. Save \n" 
	// 	 << "3. Start Over \n"
	// 	 << "3. Start a New Game \n"
	// 	 << "3. Help \n"
	// 	 << "4. Quit" << endl;

	

	return 0;

}