#include<iostream> 
#include "mines.h"

using namespace std;

Mines::Mines() {
	Point temp;
	p = temp;

	detonated = false;
	detonation_strength = 0;
}

Mines::Mines(Point p1) {
	p = p1;
	detonated = false;
	detonation_strength = 0;
}

Mines::~Mines (){

}

void Mines::set_P(char x, char y) {
	p.set_X(x);
	p.set_Y(y);
}

char Mines::get_X() {
	return p.get_X();
}

char Mines::get_Y() {
	return p.get_Y();
}

void Mines::set_is_detonated(bool d) {
	detonated = d;
}

void Mines::set_detonation_strength(double ds) {
	detonation_strength = ds;
}

bool Mines::get_is_detonated() {
	return detonated;
}

double Mines::get_detonation_strength() {
	return detonation_strength;
}

std::ostream& operator<< (std::ostream &out, Mines& m) {
    out << m.get_X() << "," << m.get_Y();
    out << ", Strength of the mine is " << m.get_detonation_strength() << endl;
    return out;
}