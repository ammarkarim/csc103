// Week 2 Q11


#include<iostream> 
using namespace std;

string convert_to_string(int current_count);
void print_lyrics(int number);


int main()
{   
    int number;

    cout << "enter number of bottles: ";
    cin >> number;

    print_lyrics(number);
  
    return 0;
}

void print_lyrics(int number) {
    for (int i=number; i > 0; i--){
        cout << convert_to_string(i) << " bottles of beer on the wall," << endl;
        cout << convert_to_string(i) << " bottles of beer," << endl;
        cout << "Take one down, pass it around" << endl;
    }
    cout << "Zero bottles of beer on the wall." << endl;
}

string convert_to_string(int current_count){
    string first_digit, second_digit;
    int div_ones, div_ten;


    //set second digit 
    div_ones = current_count % 10;
    switch(div_ones) {
        case 0:
            second_digit = "";
            break;
        case 1:
            second_digit ="One";
            break;
        case 2:
            second_digit ="Two";
            break;
        case 3:
            second_digit ="Three";
            break;
        case 4:
            second_digit ="Four";
            break;
        case 5:
            second_digit ="Five";
            break;
        case 6:
            second_digit ="Six";
            break;
        case 7:
            second_digit ="Seven";
            break;
        case 8:
            second_digit ="Eight";
            break;
        case 9:
            second_digit ="Nine";
            break;
    }

    //return if 0-9
    if (current_count > 0 && current_count < 10) {
        return second_digit;
    }

    //10-19
    if (current_count >= 10 && current_count < 20) {
        switch(current_count) {
            case 10:
              second_digit ="Ten";
              break;
            case 11:
              second_digit ="Eleven";
              break;
            case 12:
              second_digit ="Twelve";
              break;
            case 13:
              second_digit ="Thirteen";
              break;
            case 14:
              second_digit ="Fourteen";
              break;
            case 15:
              second_digit ="Fifteen";
              break;
            case 16:
              second_digit ="Sixteen";
              break;
            case 17:
              second_digit ="Seventeen";
              break;
            case 18:
              second_digit ="Eighteen";
              break;
            case 19:
              second_digit ="Nineteen";
              break;
        }
        return second_digit;
    }

    div_ten = current_count / 10;

    switch(div_ten) {
        case 2:
          first_digit ="Twenty";
          break;
        case 3:
          first_digit ="Thirty";
          break;
        case 4:
          first_digit ="Fourty";
          break;
        case 5:
          first_digit ="Fifty";
          break;
        case 6:
          first_digit ="Sixty";
          break;
        case 7:
          first_digit ="Seventy";
          break;
        case 8:
          first_digit ="Eighty";
          break;
        case 9:
          first_digit ="Ninety";
          break;
    }

    if (second_digit == "")
        return first_digit;
    else
        return first_digit + "-" + second_digit;
}