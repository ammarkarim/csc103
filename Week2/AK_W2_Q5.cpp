// Week 2 Q5


#include<iostream> 
using namespace std;

int main()
{   
    int smallest, largest, input;

    cout << "enter a value. [to exit enter 99] " << endl;
    cin >> input;

    smallest = input, largest = input;

    while (input != 99) {
        cout << "enter a value. [to exit enter 99] " << endl;
        cin >> input;
        if (input < smallest && input != 99)
            smallest = input;
        if (input > largest && input != 99)
            largest = input;
    };
    
    cout << "smallest is " << smallest << endl;
    cout << "largest is " << largest << endl;
    return 0;
}