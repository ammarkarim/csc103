#ifndef POINT_H
#define POINT_H

#include <string>
 

class Point {
    double x, y;
public:
    Point();
    Point(double,double);
    ~Point();
    void set_X(double);
    void set_Y(double);
    double get_X();
    double get_Y();
    void print();
    std::string quadrant();
    Point operator + (Point&);
    Point operator - (Point&);
};
 

#endif