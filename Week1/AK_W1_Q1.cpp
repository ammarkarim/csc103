// Week 1 Q1

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	string firstName, lastNameInitial;
	int age, bodyTemp, zipcode;
	bool alive;

    firstName = "Ammar";
    lastNameInitial = "K";
    age = 23;
    bodyTemp = 82;
    alive = true;
    zipcode = 11355;

    cout << "First Name: " << firstName << endl;
    cout << "Last Name Initial: " << lastNameInitial << endl;
    cout << "Age: " << age << endl;
    cout << "Body Temp: " << bodyTemp << endl;
    cout << "Alive: " << alive << endl;
    cout << "Zip Code: " << zipcode << endl;
	
    return 0;
}