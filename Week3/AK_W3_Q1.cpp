// Week 3 Q1


#include<iostream> 
#include <algorithm>
using namespace std;

int main()
{	
	int maxSize;
    int count=0;
    int sum=0;
    int input;
    maxSize = 5;

    int A [maxSize];

    while (count < maxSize && input != 0){
        cout << "Enter a number. enter 0 to exit" << endl;
        cin >> input;
        if (input == 0) 
            continue;

        A[count] = input;
        count++;

    }

    cout << "forward" << endl;
    for (int i=0; i < count; i++){
        cout << A[i] << endl;
        sum += A[i];
    }

    cout << "backwards" << endl;
    for (int j=count-1; j >= 0; j--){
        cout << A[j] << endl;
    }

    cout << "sum " << sum << endl;

    cout << "mean " << double(sum/count) << endl;

    cout << "max " << *max_element(A, A+count)<< endl;

    cout << "min " << *min_element(A, A+count)<< endl;


	
    return 0;
}