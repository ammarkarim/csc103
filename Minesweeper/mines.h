#ifndef MINES_H
#define MINES_H

#include <string>
#include "point.h"
 

class Mines {
    Point p;
    bool detonated;
    double detonation_strength;
public:
    Mines();
    Mines(Point);
    ~Mines();
    void set_P(char,char);
    char get_X();
    char get_Y();
    void set_is_detonated(bool);
    void set_detonation_strength(double);
    bool get_is_detonated();
    double get_detonation_strength();
    void print();
    friend std::ostream& operator << (std::ostream& out, Mines&);
};
 

#endif