#ifndef GAME_H
#define GAME_H

#include <string>
#include "game.h"
#include "map.h"
 

class Game {
    Map gameMap;
    Mines* mines;
    int numberOfMines;
    int length, width;
    int wins;
    int health;

    char lengthLabel[37] = {'A','B','C','D','E','F','G','H','I','J','K','L','M',
    'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
    char widthLabel[37] = {'A','B','C','D','E','F','G','H','I','J','K','L','M',
    'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};


public:
    Game();
    Game(int,int,int);
    ~Game();
    void newGame();
    void reset();
    void generateMines();
    bool isGameWon();
    bool healthZero();
    void enterCoord(char,char,char);
    bool checkEntered(char,char);
    char convertToXcoord(int);
    char convertToYcoord(int);
    void printMines();
};
 

#endif