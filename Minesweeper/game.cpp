#include<iostream> 
#include <stdio.h>   
#include <stdlib.h>    
#include <time.h> 
#include "game.h"

using namespace std;


Game::Game () {
    Map temp;
    gameMap = temp;
}

Game::Game (int l, int w, int minesCount) {
    length = l;
    width = w;
    numberOfMines = minesCount;
    Map temp(length, width);
    gameMap = temp;


    mines = new Mines[numberOfMines];



    generateMines();

    gameMap.generateSolution(mines, numberOfMines);



    wins = 0;
    health = 100;
}

Game::~Game () {
    // delete mines;
}

void Game::newGame(){
    generateMines();
    cout << "hello" << endl;
    health = 100;
    Map temp(length, width);
    gameMap = temp;
    gameMap.generateSolution(mines, numberOfMines);
}

void Game::reset(){
    health = 100;
    gameMap.generateCurrent();
    gameMap.generateEntered();
}

void Game::generateMines(){
    srand(time(NULL));
    int randMine;
    int size = length*width;
    char x,y;
    int count=0;

    for (int i=0; i<numberOfMines; i++) {
        randMine = rand() % size + 0;
        x = convertToXcoord(randMine);
        y = convertToYcoord(randMine);
        Point p(x,y);
        mines[count++] = Mines(p);
    }
}

void Game::enterCoord(char x, char y, char type){
    gameMap.updateCurrentMap(x,y,type);
    gameMap.updateEntered(x,y);


    if(type == 'r' && gameMap.containsMine(x,y)){
        cout << "you hit a mine. ouch!" << endl;
        cout << "your health is now " << health << endl;
        health -= 25;
    }

    gameMap.printCurrentMap();
}

bool Game::isGameWon(){
    if (gameMap.isGameWon()){
        wins += 1;
        return true;
    }
    else 
        return false;
}

bool Game::healthZero(){
    if (health == 0)
        return true;
    else
        return false;
}

bool Game::checkEntered(char x, char y){
    if(gameMap.checkIfEntered(x,y))
        return true;
    else 
        return false;
}

char Game::convertToXcoord(int mineLocation) {
    int x = mineLocation / length;
    return lengthLabel[x];
}

char Game::convertToYcoord(int mineLocation) {
    int y = mineLocation % length;
    return lengthLabel[y];
}

void Game::printMines() {
    for (int i=0; i < numberOfMines; i++) {
        cout << mines[i];
    }
}



