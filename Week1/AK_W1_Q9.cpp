// Week 1 Q9

// Header file for input output functions
#include<iostream> 

using namespace std;

// main function -
// where the execution of program begins
int main()
{	
	int a, b, c;
	int median;

	a = 9, b = 8, c = 6;

	if (a <= b) {
		if (a >= c) 
			median = a;
		else 
			if (b >= c)
				median = c;
			else 
				median = b;
	} 
	else if (c <= a) {
		if (c >= b) 
			median = c;
		else
			median = b;
	}
	else if (b <= c) {
		if (b >= a)
			median = b;
		else
			median = a;
	}
	cout << "the median is " << median << endl;
	
    return 0;
	
}